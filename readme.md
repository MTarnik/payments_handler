Customer makes a payment, transferring money on our bank account. Operators, who processe
payments on our side, download bank statements from the i-net bank every 20 minutes. Company
owns accounts in different banks and each bank has got it's own statement format.

The web page with the following functionality should be developed:
On the first page there are 2 fields – to select the bank and to upload the file (illustration 1).

In the first field there should be a drop down list with all the banks company owns account in (let&#39;s
consider there are 3 banks, names could be taken randomly).

Each bank has it's own format for the bank statement. In case operator selects bank 1 and tries to
upload statements from the bank 2, error message should appear.

When file is uploaded, it should be parsed and compared with the information stored in the database
to exclude possibility of uploading duplicated payments. Payments existing in the system should be
represented in one column, payments contained in the uploaded file should be represented in the
other column (illustration 2).

Only positive payments should be uploaded into the system (incoming payments).

Payments are matched by the combination of date, amount, account number, name and surname.
Possibility of the duplicated payment upload should be excluded. (illustration 3).
Operator should be able to put a tick in the check-box near the payments which should be uploaded
into the system. Also there should be possibility to tick / untick all.

When payments on the bank statement side are ticked and “Submit” button is pressed, payments
should be uploaded into the system (saved in database).


Information about the payments to be stored in database (minimum, additional info could be stored
as well if needed).
Payment ID in the system
Payment date from the file
Timestamp when payment was uploaded into the system
Payment amount
Bank account money was paid from
Purpose of the payment
Account owner's name and surname


Example from the file:
00:00:00|02/12/2015|51114010100000538754001001|2330,00|PLN|772 Przelew wewnźtrzny; z
rach.: 40114020040000390274000003; od: MICHA£ JAGUSIAK UL.MAKOWA 19 44-142
RYBNIK; tyt.: SPLATA POZYCZKI ID KLIENTA 1333118 ; TNR:
175031066110755.120001|772||02/12/2015|175031076617055.120001

02/12/2015 – payment date
2330,00 – amount
40114020040000390274000003 – bank account money was paid from
SPLATA POZYCZKI ID KLIENTA 1333118 – purpose of the payment
MICHA£ – name
JAGUSIAK – suname

Technologies to be used: PHP 7.x (pure PHP, no framework), jquery, apache, mysql. Encoding for
DB UTF-8. MySQL DB.


#### Non functional requirements:
- Compatibility with PHP 7.x configured as Apache module
- Pure PHP code without any frameworks or external libraries
- Connection to the database using mysqli extension only
- Files and DB encoding UTF-8 only
- Acceptable use of jQuery and plugins for jQuery
- Configuration should be placed in a separate file

#### Expected result:
archive with the code + set up description + SQL file for creating and configuring DB.

