drop table transactions;

CREATE TABLE transactions (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  upload_timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
  payment_id VARCHAR(60) NOT NULL, -- tnr
  payment_date DATE NOT NULL,
  payment_amount DECIMAL(9,2) NOT NULL,
  payment_currency CHAR(3) NOT NULL DEFAULT 'PLN',
  payment_bank_account_sender CHAR(32) NOT NULL,
  payment_bank_account_receiver CHAR(32) NOT NULL,
  payment_bank_account_receiver_nrb CHAR(8) NOT NULL, -- it could be done by trigger
  payment_sender_name VARCHAR (255) NOT NULL,
  payment_sender_surname VARCHAR (255) NOT NULL,
  payment_purpose TEXT,
  payment_tnr varchar(30)
);

create index transactions_payment_id_index on transactions (payment_id);
create index transactions_bank_acc_receiver_nrb_index on transactions (payment_bank_account_receiver_nrb);
create index transactions_payment_date_index on transactions (payment_date);
create index transactions_tnr_index on transactions (payment_tnr);
create index transactions_search_index on transactions (payment_date, payment_amount, payment_currency, payment_bank_account_sender, payment_sender_name, payment_sender_surname);

INSERT INTO transactions (
  payment_id, payment_date, payment_amount, payment_bank_account_sender,
  payment_bank_account_receiver, payment_sender_name, payment_sender_surname,
  payment_purpose, payment_tnr, payment_bank_account_receiver_nrb
) VALUES
(
  '175031006617711.380001', '2015-12-02', '0.01', '45114020040000250333300002',
  '51114010100000538754001001', 'ANNA', 'JAROSZEK',
  'POTWIERDZAM UPOWAZNIENIE VIASMS PL DO ZAPYTAN W BAZACH INFORMACJI GOSP ODARCZYCH, ID 1308893',
  '175031660117711.380001', '11401010'
),
(
  '175031076617055.120001', '2015-12-02', '2330.01', '40114020040000390274000003',
  '51114010100000538754001001', 'MICHAŁ', 'JAGUSIAK',
  'SPLATA POZYCZKI ID KLIENTA 1333118',
  '175031066110755.120001', '11401010'
),
(
  '175036073107784.210001', '2015-12-02', '270.01', '43114020170000460204100004',
  '51114010100000538754001001', 'WOJCIECH', 'WALIGÓRA',
  'PRZEDŁUŻENIE POŻYCZKI KLIENT ID 110 0010',
  '175031663017084.210001', '11401010'
)
;

update transactions set upload_timestamp=now()