<?php
namespace Utils;

use App\Config\Config;
use App\Model\Transaction\Transaction;

class Db
{
    /**
     * @var null|\mysqli
     */
    var $dbConnection = null;

    public function getConnection ()
    {
        if ($this->dbConnection) {
            return $this->dbConnection;
        }

        return $this->dbConnection = $this->connect();
    }

    private function connect ()
    {
        $dbConfig = Config::getDbConfigData();
        $mysqli = new \mysqli($dbConfig['host'], $dbConfig['user'], $dbConfig['password'], $dbConfig['dbname']);
        if ($mysqli->connect_errno) {
            throw new \Exception("Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error);
        }
        $mysqli->set_charset('utf8');
        return $mysqli;
    }

    public function searchLastTransactions ($nrb)
    {
        $result = [];
        $q='SELECT *
            FROM transactions
            WHERE upload_timestamp > DATE_ADD(NOW(), INTERVAL -1 HOUR)
                AND payment_bank_account_receiver_nrb = ?
            ORDER BY upload_timestamp DESC';
        if ($stmt = $this->getConnection()->prepare($q)) {
            $stmt->bind_param("s", $nrb);
            $stmt->execute();
            $res = $stmt->get_result();
            while ($tr = $res->fetch_object(Transaction::class)) {
                $result[] = $tr;
            };
            $stmt->close();
        }
        return $result;
    }


    public function saveTransaction ($transaction)
    {
        $columnNameList = ['payment_id', 'payment_date', 'payment_amount', 'payment_bank_account_sender',
            'payment_bank_account_receiver', 'payment_sender_name', 'payment_sender_surname',
            'payment_purpose', 'payment_tnr', 'payment_bank_account_receiver_nrb'];
        $valueList = [];
        foreach ($columnNameList as $columnName) {
            if (isset($transaction[$columnName])) {
                $valueList[$columnName] = $this->getConnection()->real_escape_string($transaction[$columnName]);
            }
        }
        $sql = "INSERT INTO transactions (" . join(', ', array_keys($valueList)) . ")
                VALUES ('" . join("', '" , $valueList) . "')";
        return $this->getConnection()->query($sql);
    }
}