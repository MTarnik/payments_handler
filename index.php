<?php
spl_autoload_register();

$controller = new App\Controller\Import(new App\View\View());

$_POST['action'] = isset($_POST['action']) ? $_POST['action'] : '';

switch ($_POST['action']) {
    case 'importPayments':
        // we've got transactions to save!
        try {
            $controller->saveTransactions();
        } catch (Exception $exception) {
            error_log($exception->getMessage());
        }
        break;
    case 'comparePayments':
        // we've got file uploaded!
        try {
            if (!isset($_FILES['transactionListFile']) || !$_FILES['transactionListFile']['tmp_name']) {
                throw new \Exception('No file attached');
            }
            if (!isset($_POST['transactionListFormat']) || !$_POST['transactionListFormat']) {
                throw new \Exception('No bank set');
            }
            $controller->listTransactions(
                (new App\Parser\ParserFactory())->getParser($_POST['transactionListFormat']),
                file_get_contents($_FILES['transactionListFile']["tmp_name"])
            );
        } catch (Exception $exception) {
            $controller->showImportForm($exception->getMessage());
            error_log($exception->getMessage());
        }
        break;
    default:
        $controller->showImportForm();
}
