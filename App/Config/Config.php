<?php
namespace App\Config;

class Config
{
    public static function getDbConfigData()
    {
        return [
            'host' => 'localhost',
            'port' => 3306,
            'user' => 'root',
            'password' => '',
            'dbname' => 'viasms'
        ];
    }

    public static function getBankList()
    {
        return [
            "Alior" => [
                "code" => "alior",
                "nrb" => "24902225",
                "name" => "Alior",
                "format" => ["csv"]
            ],
            "Pkosa" => [
                "code" => "pkosa",
                "nrb" => "12402224",
                "name" => "PKO SA",
                "format" => ["csv"]
            ],
            "Pkobp" => [
                "code" => "pkobp",
                "nrb" => "10202223",
                "name" => "PKO BP",
                "format" => ["xml"]
            ],
            "Millenium" => [
                "code" => "millenium",
                "nrb" => "11602221",
                "name" => "Millenium",
                "format" => ["csv"]
            ],
            "Bzwbk" => [
                "code" => "bzwbk",
                "nrb" => "10902221",
                "name" => "Bank Zachodni WBK SA",
                "format" => ["csv"]
            ],
            "Ing" => [
                "code" => "ing",
                "nrb" => "10502220",
                "name" => "ING Bank",
                "format" => ["mt942", "xml"]
            ],
            "Bre" => [
                "code" => "bre",
                "nrb" => "11401010",
                "name" => "BRE Bank SA",
                "format" => ["dat"]
            ]
        ];
    }

    public static function getTemplatePath()
    {
        return dirname(__FILE__) . '/../templates/';
    }
}
