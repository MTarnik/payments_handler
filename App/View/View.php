<?php
namespace App\View;

use App\Config\Config;

class View implements ViewInterface {

    /**
     * @param string $templateName
     * @param mixed $data
     */
    public function render(string $templateName, $data): void
    {
        $pageTitle = isset($data['pageTitle']) ? : $templateName;
        ob_start();
        include Config::getTemplatePath() . $templateName . '.phtml';
        $pageBody = ob_get_contents();
        ob_clean();
        include Config::getTemplatePath() . 'layout.phtml';
        $page = ob_get_contents();
        ob_end_clean();
        echo $page;
    }
}