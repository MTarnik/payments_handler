<?php
namespace App\View;

interface ViewInterface
{
    /**
     * @param string $templateName
     * @param mixed $data
     */
    public function render(string $templateName, $data): void;
}