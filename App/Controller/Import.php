<?php
namespace App\Controller;

use App\Config\Config;
use App\Model\Transaction\Transaction;
use App\Parser\ParserInterface;
use App\View\View;
use Utils\Db;

class Import
{
    private $view;

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    /**
     * @param ParserInterface $parser
     * @param string $transactions
     *
     * @throws \Exception
     */
    public function listTransactions(ParserInterface $parser, string $transactions)
    {
        $transactionList = $parser->getTransactionList($transactions);
        if (!count($transactionList)) {
            throw new \Exception('No data to import');
        }
        if (!$parser->isValidData($transactionList[0])) {
            $errMsg = 'This file seems to be not valid for this Bank';
            throw new \Exception($errMsg);
        }

        $db = new Db();
        $existingTransactionList = $db->searchLastTransactions($parser->getBankNrb());

        $existingTransactionList = $this->findExistingTransactions($existingTransactionList, $transactionList);
        $newTransactionList = $this->findNotExistingTransactions($transactionList, $existingTransactionList);

        $this->view->render(
            'comparePayments',
            ['existingTransactionList' => $existingTransactionList, 'newTransactionList' => $newTransactionList]
        );
    }

    /**
     * @param Transaction[] $aTransactionList
     * @param Transaction[] $bTransactionList
     * @return Transaction[]
     */
    private function findExistingTransactions($aTransactionList = [], $bTransactionList = [])
    {
        return array_filter(
            $aTransactionList,
            function($aTransaction) use ($bTransactionList) {
                foreach ($bTransactionList as $i => $bTransaction) {
                    if ($aTransaction->sameAs($bTransaction)) {
                        return true;
                    }
                }
            });
    }

    /**
     * @param Transaction[] $aTransactionList
     * @param Transaction[] $bTransactionList
     * @return Transaction[]
     */
    private function findNotExistingTransactions($aTransactionList = [], $bTransactionList = [])
    {
        return array_filter(
            $aTransactionList,
            function($aTransaction) use ($bTransactionList) {
                foreach ($bTransactionList as $i => $bTransaction) {
                    if ($aTransaction->sameAs($bTransaction)) {
                        return false;
                    }
                    return true;
                }
            });
    }

    /**
     * @param string $msg
     */
    public function showImportForm ($msg = '')
    {
        $this->view->render('importPayments', ['bankList' => Config::getBankList(), 'errMsg' => $msg]);
    }

    public function saveTransactions ()
    {
        if (!isset($_POST['newTransaction'])) {
            return;
        }
        $toSaveTransactionList = array_filter(
            $_POST['newTransaction'],
            function($t) { return isset($t['import']) && (int)$t['import']; }
        );
        $db = new Db();
        $saveResult = [];
        foreach ($toSaveTransactionList as $tnr => $transaction) {
            $saveResult[$tnr] = $db->saveTransaction($transaction);
        }
        $this->view->render('saveSummary', ['saveResult' => $saveResult]);
    }
}