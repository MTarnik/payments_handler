<?php
namespace App\Model;

class Model
{
    static $tablename;

    /**
     * @return string
     */
    static function getTablename ()
    {
        return self::$tablename;
    }
}