<?php
namespace App\Model\Transaction;

Interface TransactionInterface
{
    //public function get(): Transaction;
    public function getPaymentId(): string;
    public function setPaymentId(string $paymentId);
    public function getPaymentDate(): string;
    public function setPaymentDate(string $value);
    public function getPaymentAmount(): float;
    public function setPaymentAmount(string $value);
    public function getPaymentCurrency(): string;
    public function setPaymentCurrency(string $value);
    public function getPaymentBankAccountSender(): string;
    public function setPaymentBankAccountSender(string $value);
    public function getPaymentBankAccountReceiver(): string;
    public function setPaymentBankAccountReceiver(string $value);
    public function getPaymentBankAccountReceiverNrb(): string;
    public function setPaymentBankAccountReceiverNrb(string $value);
    public function getPaymentSenderName(): string;
    public function setPaymentSenderName(string $value);
    public function getPaymentSenderSurname(): string;
    public function setPaymentSenderSurname(string $value);
    public function getPaymentPurpose(): string;
    public function setPaymentPurpose(string $value);

    public function sameAs(TransactionInterface $transaction): bool;
}