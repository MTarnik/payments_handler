<?php
namespace App\Model\Transaction;

use App\Model\Model;

class Transaction extends Model implements TransactionInterface
{
    private $tableName = 'transactions';

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var \DateTime
     */
    protected $upload_timestamp;

    /**
     * @var string
     */
    protected $payment_id;

    /**
     * @var \DateTime
     */
    protected $payment_date;

    /**
     * @var float
     */
    protected $payment_amount;

    /**
     * @var string
     */
    protected $payment_currency;

    /**
     * @var string
     */
    protected $payment_bank_account_sender;

    /**
     * @var string
     */
    protected $payment_bank_account_receiver;

    /**
     * @var string
     */
    protected $payment_bank_account_receiver_nrb;

    /**
     * @var string
     */
    protected $payment_sender_name;

    /**
     * @var string
     */
    protected $payment_sender_surname;

    /**
     * @var string
     */
    protected $payment_purpose;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getUploadTimestamp()
    {
        return $this->upload_timestamp;
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->payment_id;
    }

    /**
     * @param string $payment_id
     * @return Transaction
     */
    public function setPaymentId($payment_id)
    {
        $this->payment_id = $payment_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentDate(): string
    {
        return $this->payment_date;
    }

    /**
     * @param string $payment_date
     * @return Transaction
     */
    public function setPaymentDate(string $payment_date)
    {
        $this->payment_date = $payment_date;
        return $this;
    }

    /**
     * @return float
     */
    public function getPaymentAmount(): float
    {
        return $this->payment_amount;
    }

    /**
     * @param float $payment_amount
     * @return Transaction
     */
    public function setPaymentAmount($payment_amount)
    {
        $this->payment_amount = $payment_amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentCurrency(): string
    {
        return $this->payment_currency;
    }

    /**
     * @param string $payment_currency
     * @return Transaction
     */
    public function setPaymentCurrency($payment_currency)
    {
        $this->payment_currency = $payment_currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentBankAccountSender(): string
    {
        return $this->payment_bank_account_sender;
    }

    /**
     * @param string $payment_bank_account_sender
     * @return Transaction
     */
    public function setPaymentBankAccountSender($payment_bank_account_sender)
    {
        $this->payment_bank_account_sender = $payment_bank_account_sender;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentBankAccountReceiver(): string
    {
        return $this->payment_bank_account_receiver;
    }

    /**
     * @param string $payment_bank_account_receiver
     * @return Transaction
     */
    public function setPaymentBankAccountReceiver($payment_bank_account_receiver)
    {
        $this->payment_bank_account_receiver = $payment_bank_account_receiver;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentBankAccountReceiverNrb(): string
    {
        return $this->payment_bank_account_receiver_nrb;
    }

    /**
     * @param string $payment_bank_account_receiver_nrb
     * @return Transaction
     */
    public function setPaymentBankAccountReceiverNrb($payment_bank_account_receiver_nrb)
    {
        $this->payment_bank_account_receiver_nrb = $payment_bank_account_receiver_nrb;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentSenderName(): string
    {
        return $this->payment_sender_name;
    }

    /**
     * @param string $payment_sender_name
     * @return Transaction
     */
    public function setPaymentSenderName($payment_sender_name)
    {
        $this->payment_sender_name = $payment_sender_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentSenderSurname(): string
    {
        return $this->payment_sender_surname;
    }

    /**
     * @param string $payment_sender_surname
     * @return Transaction
     */
    public function setPaymentSenderSurname($payment_sender_surname)
    {
        $this->payment_sender_surname = $payment_sender_surname;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentPurpose(): string
    {
        return $this->payment_purpose;
    }

    /**
     * @param string $payment_purpose
     * @return Transaction
     */
    public function setPaymentPurpose($payment_purpose)
    {
        $this->payment_purpose = $payment_purpose;
        return $this;
    }

    public function sameAs(TransactionInterface $transaction): bool
    {
        return $this->getPaymentDate() == $transaction->getPaymentDate()
            && $this->getPaymentAmount() == $transaction->getPaymentAmount()
            && $this->getPaymentBankAccountSender() == $transaction->getPaymentBankAccountSender()
            && $this->getPaymentSenderName() == $transaction->getPaymentSenderName()
            && $this->getPaymentSenderSurname() == $transaction->getPaymentSenderSurname()
            ;
    }
}