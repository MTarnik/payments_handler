<?php
namespace App\Parser;

use App\Model\Transaction\TransactionInterface;

interface ParserInterface
{
    /**
     * @param TransactionInterface $transaction
     * @return bool
     */
    public function isValidData(TransactionInterface $transaction): bool;

    /**
     * @param string $transactions
     * @return TransactionInterface[]
     */
    public function getTransactionList (string $transactions): array;

    /**
     * @return string
     */
    public function getBankCodeName(): string;

    /**
     * @return string
     */
    public function getBankFormatName(): string;

    /**
     * @return string
     */
    public function getCodeFormatName(): string;

    /**
     * @return string
     */
    public function getBankNrb(): string;
}