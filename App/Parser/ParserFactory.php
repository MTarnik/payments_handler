<?php
namespace App\Parser;


class ParserFactory
{
    public function getParser(string $transactionListFormatName)
    {
        $transactionListFormat = explode('--', $transactionListFormatName);
        $parserName = 'App\\Parser\\' . ucfirst($transactionListFormat[1]) . '\\' . ucfirst($transactionListFormat[0]);
        try {
            return new $parserName();
        } catch (\Exception $exception) {
            error_log($exception->getMessage());
            return null;
        }
    }
}