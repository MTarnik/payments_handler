<?php
namespace App\Parser;


abstract class Parser implements ParserInterface
{
    /**
     * @var array
     */
    protected $transactionData;

    /**
     * @var string
     */
    protected $bankCodeName;

    /**
     * @var string
     */
    protected $bankFormatName;

    /**
     * @var string
     */
    protected $bankNrb;

    /**
     * @return array
     */
    public function getTransactionData(): array
    {
        return $this->transactionData;
    }

    /**
     * @param array $transactionData
     * @return self
     */
    public function setTransactionData(array $transactionData): self
    {
        $this->transactionData = $transactionData;
        return $this;
    }

    /**
     * @return string
     */
    public function getBankCodeName(): string
    {
        return $this->bankCodeName;
    }

    /**
     * @return string
     */
    public function getBankNrb(): string
    {
        return $this->bankNrb;
    }

    /**
     * @return string
     */
    public function getBankFormatName(): string
    {
        return $this->bankFormatName;
    }

    /**
     * @return string
     */
    public function getCodeFormatName(): string
    {
        return join('--', [$this->getBankCodeName(), $this->getBankFormatName()]);
    }

}