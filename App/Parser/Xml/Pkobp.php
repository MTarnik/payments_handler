<?php
namespace App\Parser\Xml;

use App\Model\Transaction\Transaction;
use App\Model\Transaction\TransactionInterface;
use App\Parser\Parser;

class Pkobp extends Parser
{
    protected $bankCodeName = 'pkobp';
    protected $bankFormatName = 'xml';
    protected $bankNrb = '10202223';

    public function getTransactionList (string $transactions): array
    {
        return [new Transaction()];
    }

    public function isValidData(TransactionInterface $transaction): bool
    {
        return false;
    }
}