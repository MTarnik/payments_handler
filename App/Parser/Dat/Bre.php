<?php
namespace App\Parser\Dat;

use App\Config\Config;
use App\Model\Transaction\Transaction;
use App\Model\Transaction\TransactionInterface;
use App\Parser\Parser;

class Bre extends Parser
{
    protected $bankCodeName = 'bre';
    protected $bankFormatName = 'dat';
    protected $bankNrb = '11401010';

    private $transactionColumns = [
        'time', 'payment_date', 'payment_bank_account_receiver', 'payment_amount', 'payment_currency',
        'description', 'unknown1', 'unknown2', 'transaction_date_2', 'payment_id'
    ];

    private $descriptionColumns = [
        'operation_type', 'payment_bank_account_sender', 'sender', 'payment_purpose', 'payment_id'
    ];

    public function getTransactionList (string $transactions): array
    {
        $transactionList = str_getcsv($transactions, "\n");
        return array_map(function ($transactionLine) {
            $transaction = array_combine($this->transactionColumns, str_getcsv($transactionLine, '|'));
            $transactionDesc = array_combine($this->descriptionColumns, str_getcsv($transaction['description'], ';'));
            foreach ($transactionDesc as $key => $value) {
                $transactionDesc[$key] = trim(preg_replace('/^([a-zA-Z \.]+):\s?/', '', $value));
            }
            $sender = preg_split('/  /', $transactionDesc['sender']);
            $sender = explode(' ', count($sender) == 1 ? $transactionDesc['sender'] : $sender[0]);

            return (new Transaction())
                ->setPaymentId($transaction['payment_id'])
                ->setPaymentAmount(floatval(str_replace(',', '.', $transaction['payment_amount'])))
                ->setPaymentCurrency($transaction['payment_currency'])
                ->setPaymentPurpose($transactionDesc['payment_purpose'])
                ->setPaymentDate(\DateTime::createFromFormat('d/m/Y', $transaction['payment_date'])->format('Y-m-d'))
                ->setPaymentSenderName($sender[0])
                ->setPaymentSenderSurname($sender[1])
                ->setPaymentBankAccountReceiver($transaction['payment_bank_account_receiver'])
                ->setPaymentBankAccountSender($transactionDesc['payment_bank_account_sender'])
                ->setPaymentBankAccountReceiverNrb(substr($transaction['payment_bank_account_receiver'],2 , 8))
                ;
        }, $transactionList);
    }

    public function isValidData(TransactionInterface $transaction): bool
    {
        $transactionReceiverBankAccNrb = substr($transaction->getPaymentBankAccountReceiver(), 2, 8);
        $bankList = array_filter(
            Config::getBankList(),
            function($bank) use ($transactionReceiverBankAccNrb) {
                return $bank['code'] == $this->bankCodeName
                    && in_array($this->bankFormatName, $bank['format'])
                    && $transactionReceiverBankAccNrb == $bank['nrb']
                ;
            }
        );
        return count($bankList) == 1;
    }
}