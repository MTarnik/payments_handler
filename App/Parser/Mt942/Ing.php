<?php
namespace App\Parser\Mt942;

use App\Model\Transaction\Transaction;
use App\Model\Transaction\TransactionInterface;
use App\Parser\Parser;

class Ing extends Parser
{
    protected $bankCodeName = 'ing';
    protected $bankFormatName = 'mt942';
    protected $bankNrb = '10502220';

    public function getTransactionList (string $transactions): array
    {
        return [new Transaction()];
    }

    public function isValidData(TransactionInterface $transaction): bool
    {
        return false;
    }
}