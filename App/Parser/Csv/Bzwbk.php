<?php
namespace App\Parser\Csv;

use App\Model\Transaction\Transaction;
use App\Model\Transaction\TransactionInterface;
use App\Parser\Parser;

class Bzwbk extends Parser
{
    protected $bankCodeName = 'bzwbk';
    protected $bankFormatName = 'csv';
    protected $bankNrb = '10902221';

    public function getTransactionList (string $transactions): array
    {
        return [new Transaction()];
    }

    public function isValidData(TransactionInterface $transaction): bool
    {
        return false;
    }
}