<?php
namespace App\Parser\Csv;

use App\Model\Transaction\Transaction;
use App\Model\Transaction\TransactionInterface;
use App\Parser\Parser;

class Alior extends Parser
{
    protected $bankCodeName = 'alior';
    protected $bankFormatName = 'csv';
    protected $bankNrb = '24902225';

    public function getTransactionList (string $transactions): array
    {
        return [new Transaction()];
    }

    public function isValidData(TransactionInterface $transaction): bool
    {
        return false;
    }
}