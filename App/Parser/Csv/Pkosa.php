<?php
namespace App\Parser\Csv;

use App\Model\Transaction\Transaction;
use App\Model\Transaction\TransactionInterface;
use App\Parser\Parser;

class Pkosa extends Parser
{
    protected $bankCodeName = 'pkosa';
    protected $bankFormatName = 'csv';
    protected $bankNrb = '12402224';

    public function getTransactionList (string $transactions): array
    {
        return [new Transaction()];
    }

    public function isValidData(TransactionInterface $transaction): bool
    {
        return false;
    }
}