<?php
namespace App\Parser\Csv;

use App\Model\Transaction\Transaction;
use App\Model\Transaction\TransactionInterface;
use App\Parser\Parser;

class Millenium extends Parser
{
    protected $bankCodeName = 'millenium';
    protected $bankFormatName = 'csv';
    protected $bankNrb = '11602221';

    public function getTransactionList (string $transactions): array
    {
        return [new Transaction()];
    }

    public function isValidData(TransactionInterface $transaction): bool
    {
        return false;
    }
}